#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings





// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char password[PASS_LEN];
    char hash[HASH_LEN];
};

int crackFunc(const void *a, const void *b)
{
    return strcmp(a, ((struct entry *)b)->hash);
}

int hashCompare(const void *target, const void *elem)
{
    return strcmp((*(struct entry *)target).hash, (*(struct entry *)elem).hash);
}


int file_length(char *filename)
{

    struct stat info;
    int ret = stat(filename, &info);
    if(ret == -1)
    {
        return -1;
    }
    else
    {
        return info.st_size;
    }

}


// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.

struct entry *read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    char *p = malloc(len);
    
    FILE *f = fopen(filename,"r");
    if(!f)
    {
        printf(" Can't open %s for reading.\n", filename);
        exit(1);
    }

    fread(p, sizeof(char), len, f);
    fclose(f);
    
    int num = 0;
    
    for(int i = 0; i < len; i++)
    {
        if(p[i] == '\n')
        {
            p[i] = '\0';
            num++;
        }
    }
    
    // Allocate space for array of entry structs
     struct entry *ps = malloc(num * sizeof(struct entry));
     
     // Load array with entry info
     char *foobar = &p[0];
    char *wingDing = md5(foobar, strlen(foobar));
    strcpy(ps[0].password, foobar);
    strcpy(ps[0].hash, wingDing);
     
    int j = 1;
    for(int i = 0; i < len-1; i++)
    {
        if (p[i] == '\0')
        {
            char *something = &p[i+1];
            char *hashed = md5(something, strlen(something));
            strcpy(ps[j].password, something);
            strcpy(ps[j].hash, hashed);
            j++;
        }
         
    }
    
    *size = num;
    return ps;
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen = 0;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dlen, sizeof(struct entry), hashCompare);

    // TODO
    // Open the hash file for reading.
    
    FILE *fp = fopen(argv[1],"r");
    if (!fp)
    {
        printf("Can't open %s for reading.\n", argv[1]);
        exit(1);
    }
    
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    
    //struct entry *searcht = bsearch(line, dict, dlen, sizeof(struct entry), crackFunc);
    
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    
    int count = 0;
    char line[100];
    while(fgets(line, 100, fp) != NULL)
    {
        if (line[strlen(line)-1] == '\n')
        {
            line[strlen(line)-1] = '\0';
        }
        
         struct entry *searcht = bsearch(line, dict, dlen, sizeof(struct entry), crackFunc);
        
     if( searcht != NULL)
    {
        printf("%s %s\n", searcht->password, line);
        count++;
    }
    
}
printf("Passwords cracked: %d\n", count);
}
